//
//  KanobuNetworkEngine.m
//  Kanobu Video Client
//
//  Created by Denis on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "KanobuNetworkEngine.h"
@implementation KanobuNetworkEngine
@synthesize imagesForURLs;
-(id)init
{
    self = [super init];
    self.imagesForURLs = [[NSMutableDictionary alloc] init];
    return self;
}

-(NSString*)cacheDirectoryName {

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *cacheDirectoryName = [documentsDirectory stringByAppendingPathComponent:@"KanobuVideoThumbnailsCache"];
    return cacheDirectoryName;
}



@end
