//
//  AsyncImageView.m
//  Kanobu Video Client
//
//  Created by Denis on 6/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AsyncImageView.h"


@implementation AsyncImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    NSAssert(NO,@"method unsupported");
    return self;
}

- (id)initWithFrame:(CGRect)frame andImagePlaceholder:(UIImage*)image
{
    self = [super initWithFrame:frame];
    
    if (self && image!=nil) {
        mainImageView = [[UIImageView alloc] initWithImage:image];
    }
    else {
        mainImageView = [[UIImageView alloc] initWithFrame:frame];
    }
    [self addSubview:mainImageView];
    return self;
}

- (void)loadImageFromURL:(NSURL*)downloadUrl byNetwork:(MKNetworkEngine*)network{
    [network imageAtURL:downloadUrl onCompletion:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
        if (fetchedImage != nil)
        {
            NSLog(@"Image fetched sucsessfully!");
            mainImageView.image = fetchedImage;
        }
    }];
}

- (UIImage*) image {
    UIImageView* iv = [[self subviews] objectAtIndex:0];
    return [iv image];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
